# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%

import os
import pickle
import pprint
import re
import sys
import time
import traceback
from inspect import stack
from os.path import split

import requests
# %%
###############
# Libraries
###############
from bs4 import BeautifulSoup as bs
from IPython.display import HTML, display

#from .card import card

heal_regex = r'(?P<allies0>all *allies *for *)?(?P<heal>[0-9,]+) *(health|healing)( *(((to|on) *self *)?(?P<allies1>(and|to) *all *allies *))?(over *(?P<rounds>[1-9]) *round(s)?(?P<allies2> *(and|to) *all *allies)?)?)?( *((to|on) *self *)?(?P<allies3>(and|to) *all *allies *))?'


def clean_img_text(txt):
    if txt == None: return txt
    txt = re.sub(r'\s*\(Icon\)\s*', ' ', txt)
    return re.sub(r'\.png\s*', ' ', txt)
class Card:
    def __init__(self, opts={}):
        self.name        = opts.get('name')
        self.accuracy    = clean_img_text(opts.get('accuracy'))
        self.description = clean_img_text(opts.get('description'))
        self.pip_cost    = clean_img_text(opts.get('pip cost'))
        self.school      = clean_img_text(opts.get('school'))
        self.shadow_cost = clean_img_text(opts.get('shadow pip cost'))
        self.type        = clean_img_text(opts.get('type'))
        self.acquisition = opts.get('acquisition')
        if type(self.shadow_cost) != 'int': self.shadow_cost = 0

    def __str__(self):
        out =  f'name: {self.name}\n'
        out += f'\taccuracy: {self.accuracy}\n'
        out += f'\tdescription: {self.description}\n'
        out += f'\tpip_cost: {self.pip_cost}\n'
        out += f'\tschool: {self.school}\n'
        out += f'\tshadow_cost: {self.shadow_cost}\n'
        out += f'\ttype: {self.type}'
        return out


HOST = "https://www.wizard101central.com"
IMG_PATTERN = r"""<img\s*(?:alt=\"(?P<alt>[^>\"]+)\")[^>]*>"""
pp = pprint.PrettyPrinter(indent=4)

# %%
###############
# Functions
###############
def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    import unicodedata
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub('[^\w\s-]', '', str(value)).strip().lower()
    value = re.sub('[-\s]+', '-', str(value))
    # ...
    return value

def store_session_cookies(session):
    with open('cookie-store.pickle', 'w+b') as f:
        pickle.dump(session.cookies, f)

def restore_session_cookies(session):
    if os.path.isfile('cookie-store.pickle'):
        with open('cookie-store.pickle', 'rb') as f:
            session.cookies.update(pickle.load(f))

def filter_illegal_cards(cards):
    for card in cards:
        pass


def imgs_to_alt_text(html):
    for match in re.finditer(IMG_PATTERN, html):
        html = html.replace(match.group(), f' {match.group(1)} ', 1)
    return html

def extract_spell_info_table(table):
    data = {}
    rows = table.find_all('tr')
    for i in range(len(rows)):
        col = rows[i].find_all('td')
        if col[0].text == 'Description':
            # special case where seperated over 2 rows
            # next row will be ignored
            data['description'] = rows[i+1].find('td').text.strip()
        elif len(col) == 2:
            # standard keypair
            data[col[0].text.strip().lower()] = col[1].text.strip()
    return data

def spell_is_aquirable(table):
    if table is None:
        return False
    text = table.text
    if "Polymorphed Wizards only" in text:
        return False
    return True
    



# %%
###############
# Scrape card URLs
###############
session = requests.session()
restore_session_cookies(session)
headers = {
    'Host': 'www.wizard101central.com',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive'
}


cards_page_text = session.get(HOST + '/wiki/Category:Healing_Spells', headers=headers).text
time.sleep(1)
cards_page = bs(cards_page_text, 'html.parser')
cards_container = cards_page.find(id='mw-pages')
card_urls = list(map(lambda x: x['href'], cards_container.find_all('a')))


# %%
##############
# Load card pages
##############


cards = []
card_pages = []
for i in range(len(card_urls)):
    print(f'\r({i+1}/{len(card_urls)})', end='')
    name = f'store/{slugify(card_urls[i])}.html'
    if os.path.isfile(name):
        txt = open(name, 'r').read()
        card_pages.append(bs(txt, 'html.parser'))
    else:
        with open(f'store/{slugify(card_urls[i])}.html', 'w+') as f:
            txt = imgs_to_alt_text(session.get(HOST + card_urls[i]).text)
            f.write(txt)
            card_pages.append(bs(txt, 'html.parser'))
        time.sleep(1)
print('\rdone')


# %%
###############
# Scrape card pages
###############
for p in card_pages:
    try:
        table = p.select('.data-table')[1]
        card = Card(extract_spell_info_table(table))
        card.name = p.text[0:60].split(' - ')[0].strip()
        if card.name.startswith("ItemCard") or card.name.startswith("TreasureCard"):
            print('invalid by type')
            continue
        if not spell_is_acquirable(p.select('.data-table')[3]):
            print('invalid by acquisition')
            continue
        print(card)
        
        cards.append(card)
    except Exception as e:
        print(f'{len(p.select(".data-table"))}::Cannot process: {p.text[0:60].strip()}')


# %%


store_session_cookies(session)


# %%
card_pages[3].select('.data-table')



# %%
