from enum import Enum, auto
import re



class Card:
    def __init__(self, opts={}):
        self.card_type   = 
        self.accuracy    = clean_img_text(opts.get('accuracy'))
        self.description = clean_img_text(opts.get('description'))
        self.pip_cost    = clean_img_text(opts.get('pip cost'))
        self.school      = clean_img_text(opts.get('school'))
        self.shadow_cost = clean_img_text(opts.get('shadow pip cost', 0))
        self.spell_type  = clean_img_text(opts.get('type'))

    def __str__(self):
        out = 'Card\n'
        out += f'\taccuracy: {self.accuracy}\n'
        out += f'\tdescription: {self.description}\n'
        out += f'\tpip_cost: {self.pip_cost}\n'
        out += f'\tschool: {self.school}\n'
        out += f'\tshadow_cost: {self.shadow_cost}\n'
        out += f'\ttype: {self.type}'
        return out

    class Effect:
        
        def __init__(self):
            pass

    
    
    class Target(Enum):
        ALL_ALLIES      = auto()
        ALL_ENEMIES     = auto()
        SINGLE_ENEMY    = auto()
        SINGLE_FRIENDLY = auto()
        SPLIT_ENEMY     = auto()

    class School(Enum):
        FIRE = auto()
        ICE = auto()
        STORM = auto()
        MYTH = auto()
        LIFE = auto()
        DEATH = auto()
        BALANCE = auto()
        STAR = auto()
        MOON = auto()
        SUN = auto()
        SHADOW = auto()
