import re
import requests

def clean_img_text(txt):
    if txt == None: return txt
    txt = re.sub(r'\s*\(Icon\)\s*', ' ', txt)
    return re.sub(r'\.png\s*', ' ', txt)


# %%
###############
# Functions
###############
def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    import unicodedata
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub('[^\w\s-]', '', str(value)).strip().lower()
    value = re.sub('[-\s]+', '-', str(value))
    # ...
    return value